const Discord = require('discord.js');
const fs = require('fs');

const client = new Discord.Client({ intents: [Discord.GatewayIntentBits.Guilds] });

//const RichEmbed = new Discord.RichEmbed();
const cfg = require('./config.json');

let quotes = [];
let fandomList = [];

for(var i = 0; i < cfg.fandoms.length; ++i) {
  var json = cfg.fandoms[i];
  fandomList.push(json.name);
}

console.log(fandomList)
var choices = [];
for(var fandom of cfg.fandoms){
  choices.push({name: fandom.displayName, value: fandom.name, description: `Get a quote from ${fandom.name}.`})
}

const commands = [
  {
    name: 'quote',
    description: 'Sends a quote.',
    options: [
      {
        name: "fandom",
        description: "Sends a random quote from a fandom of your choice.",
        type: 3, // Type of input from user: https://discord.com/developers/docs/interactions/slash-commands#applicationcommandoptiontype
        required: false,
        choices: choices
      }
    ]

  },
];

const rest = new Discord.REST({ version: '10' }).setToken(cfg.token);

(async () => {
  try {
    console.log('Started refreshing application (/) commands.');

    await rest.put(Discord.Routes.applicationCommands('601934323087835136'), { body: commands });

    console.log('Successfully reloaded application (/) commands.');
  } catch (error) {
    console.error(error);
  }
})();


// DISCORD JS IMPLEMENTATION

client.on('ready', () => {
  console.log(`Logged in as ${client.user.tag}!`);
});

client.on('error', err => {
  console.log(err);
});


client.on('interactionCreate', async interaction => {
  if (!interaction.isChatInputCommand()) return;

  if (interaction.commandName === 'quote') {
    var quote = { quote: "Quote not found.", id: -1, fandom: { name: "error", displayName: "Error", file: "error.quotes" } };
    console.log(interaction.options.data)
    if (interaction.options.data.length > 0) {
      quote.fandom = cfg.fandoms.find((f) => { if (f.name == interaction.options.data[0].value) { return true; } })
      quote.fandom.quotes = fs.readFileSync(quote.fandom.file, { encoding: 'utf8' }).toString().replace(/\r/g, "").replace(/<\\?\/?i>/g, "*").replace("?", "？").split('\n');
      if (quote.fandom.quotes.length <= 0) {
        msg.channel.send(`> Error: Fandom not found.`);
        return;
      }

      quote.id = random(quote.fandom.quotes.length - 1);
      quote.quote = quote.fandom.quotes[quote.id].replace(/\\n/g, "\n");

    }else{
      quote.fandom = cfg.fandoms[random(cfg.fandoms.length - 1)];
      quote.fandom.quotes = fs.readFileSync(quote.fandom.file, { encoding: 'utf8' }).toString().replace(/\r/g, "").replace(/<\\?\/?i>/g, "*").replace("?", "？").split('\n');
      quote.id = random(quote.fandom.quotes.length - 1);
      quote.quote = quote.fandom.quotes[quote.id].replace(/\\n/g, "\n");
    }


    await interaction.reply({ embeds: [buildQuoteEmbed(quote)], ephemeral: false });
  }
});

client.on('message', msg => {
  let msgl = msg.content.toLowerCase();
  let args = msgl.split(' ');
  if (args.length > 0 && args[0] == 'quote') {

    //var qf = "unknown";
    var quote = { quote: "Quote not found.", id: -1, fandom: { name: "error", displayName: "Error", file: "error.quotes" } };
    console.log("Arg0: ", args[0], "\nArg1: ", args[1]);

    if (args.length > 1 && args[1]) {
      let ab = args[1];
      if (ab.includes("#")) {
        let aba = args[1].split("#");
        quote.fandom = cfg.fandoms.find((f) => { if (f.name == aba[0]) { return true; } })
        quote.fandom.quotes = fs.readFileSync(quote.fandom.file, { encoding: 'utf8' }).toString().replace(/\r/g, "").replace(/<\\?\/?i>/g, "*").split('\n');
        if (quote.fandom.quotes.length <= 0) {
          msg.channel.send(`> Error: Fandom not found.`);
          return;
        }

        quote.id = aba[1];
        if (quote.id < 0 && quote.id >= fandom.quotes.length) {
          msg.channel.send(`> Error: Quote not found.`);
          return;
        }
        quote.quote = quote.fandom.quotes[quote.id];

      } else {
        quote.fandom = cfg.fandoms.find((f) => { if (f.name == args[1]) { return true; } })
        quote.fandom.quotes = fs.readFileSync(quote.fandom.file, { encoding: 'utf8' }).toString().replace(/\r/g, "").replace(/<\\?\/?i>/g, "*").split('\n');
        if (quote.fandom.quotes.length <= 0) {
          msg.channel.send(`> Error: Fandom not found.`);
          return;
        }

        quote.id = random(quote.fandom.quotes.length - 1);
        quote.quote = quote.fandom.quotes[quote.id];

      }
    } else {
      quote.fandom = cfg.fandoms[random(cfg.fandoms.length - 1)];
      quote.fandom.quotes = fs.readFileSync(quote.fandom.file, { encoding: 'utf8' }).toString().replace(/\r/g, "").replace(/<\\?\/?i>/g, "*").split('\n');
      quote.id = random(quote.fandom.quotes.length - 1);
      quote.quote = quote.fandom.quotes[quote.id];
    }

    if (quote.quote) {
      console.log("QF: ", quote.fandom.name, "\nQuoteID: ", quote.id);
      let quoteembed = buildQuoteEmbed(quote);


      msg.channel.send(quoteembed);
    }
  }
});

client.login(cfg.token);


// UTILITY FUNCTIONS


function buildQuoteEmbed(quote) {
  if (quote.quote) {
    console.log("QF: ", quote.fandom.name, "\nQuoteID: ", quote.id);
    let quoteembed = new Discord.EmbedBuilder()
      .setTitle(` `)
      .setDescription(`${quote.quote}`)
      .setColor(getRandomColor())
      .setAuthor({name: `${quote.fandom.displayName} #${quote.id}`, iconURL: quote.fandom.icon, url: "https://kurt.ee/#quotebot"})
      .setFooter({text: "Ultimate Quotes powered by Kurty00"})


    return quoteembed;
  }
}

function random(max) {
  return Math.floor(Math.random() * (max - 0 + 1) + 0);
}

function getRandomColor() {
  var letters = '0123456789ABCDEF';
  var color = '#';
  for (var i = 0; i < 6; i++) {
    color += letters[Math.floor(Math.random() * 16)];
  }
  return color;
}
